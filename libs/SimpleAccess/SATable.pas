unit SATable;

//  MIT License
//
//  Copyright (c) 2020 Sergey Kozubenko (SeaJay), Artem Muzichko
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.


interface
uses
  DB, ADODB, Forms, classes, SysUtils, SimpleLogger, Grids;
type
  TSATable = Class(TObject)
  private
    table_id : String;
    Logger : TSimpleLogger;
    ADOConnection : TADOConnection;
    ADOTable : TADOTable;
    function tableValuesUpdateSQL(valuesCS : String) : String;
    function tableValuesListToSQL(valuesCS : String) : String;


  protected
  public
    constructor Create(ADOConnection : TADOConnection; table_id : String; Logger : TSimpleLogger);
    procedure put(primaryKey, values : String);

    function get(key : String; field : String) : String;
    function fieldsList() : TStringList;
    function recordsList(pole : String): TStringList;
    function existValue(field, value : String) : boolean;
    function existField(field: String) : boolean;
    function keyField: String;
    function keys: TStringList;
    procedure syncToStringGrid(target_grid: TStringGrid; hide_key, titles: boolean);
    //  TO-DO
    //  procedure delete(key : String);
  published
  end;

implementation

constructor TSATable.Create(ADOConnection : TADOConnection; table_id : String; Logger : TSimpleLogger);
begin
  self.Logger := Logger;
  self.ADOConnection := ADOConnection;
  self.ADOTable := TADOTable.Create(nil);
  self.ADOTable.Connection := self.ADOConnection;
  self.table_id := table_id;
  self.ADOTable.TableName := table_id;
end;

procedure TSATable.put(primaryKey, values : String);
var
  pkey : String;
begin
  pkey := fieldsList[0];

  if (not existValue(pkey, primaryKey)) then begin
    self.logger.debug('table.put "' + self.table_id + '" key:"' + primaryKey + '" insert "' + values+ '"');
    values := pkey + '=' + primaryKey + ',' + values;
    self.ADOConnection.Execute('INSERT INTO ' + self.table_id + ' ' + tableValuesListToSQL(values));
  end else begin
    self.logger.debug('table.put "' + self.table_id + '" key:"' + primaryKey + '" update "' + values+ '"');
    self.logger.debug('SQL: UPDATE '+ self.table_id + ' SET ' + tableValuesUpdateSQL(values) + ' WHERE (' + pkey + '="' + primaryKey + '");');


    //UPDATE test_table SET test_table.[password] = "dfgdfgdrgdfgdfgufyierfiujjf:usfjiofd" WHERE (((test_table.uuid)="4656"));
    self.ADOConnection.Execute('UPDATE ' + self.table_id + ' SET ' + tableValuesUpdateSQL(values) + ' WHERE ('+self.table_id+'.' + pkey + '="' + primaryKey + '");');
  end;
end;
{
procedure TSATable.put(primaryKey, values : String);
var
  pkey : String;
begin
  pkey := fieldsList[0];

  if (not existValue(pkey, primaryKey)) then begin
    self.logger.debug('table.put "' + self.table_id + '" key:"' + primaryKey + '" insert "' + values+ '"');
    values := pkey + '=' + primaryKey + ',' + values;
    self.ADOConnection.Execute('INSERT INTO ' + self.table_id + ' ' + tableValuesListToSQL(values));
  end else begin
    self.logger.debug('table.put "' + self.table_id + '" key:"' + primaryKey + '" update "' + values+ '"');
    self.logger.debug('SQL: UPDATE '+ self.table_id + ' SET ' + tableValuesUpdateSQL(values) + ' WHERE (' + pkey + '="' + primaryKey + '");');
    self.ADOConnection.Execute('UPDATE '+ self.table_id + ' SET ' + tableValuesUpdateSQL(values) + ' WHERE (' + pkey + '="' + primaryKey + '");');
    //UPDATE test_table SET login = "d999999sss",password="ddGs" WHERE key="Val";
  end;
end;
 }
function TSATable.get(key : String; field : String): String;
var
  value, pkey : String;
  ADOQuery : TADOQuery;
begin
  pkey := fieldsList[0];
  self.logger.debug('table.get "' + self.table_id + '" key:"' + key + '" field "' + field+ '"');
  ADOQuery := TADOQuery.Create(nil);
  ADOQuery.Connection := self.ADOConnection;
  ADOQuery.SQL.Clear;
  self.logger.debug('table.get SQL :"' + 'SELECT '+self.table_id+'.'+field+' FROM '+self.table_id+' WHERE ((('+self.table_id+'.'+pkey+')="'+key+'"));');
  ADOQuery.SQl.Add('SELECT '+self.table_id+'.'+field+' FROM '+self.table_id+' WHERE ((('+self.table_id+'.'+pkey+')="'+key+'"));');
  ADOQuery.Active := true;
  ADOQuery.ExecSQL;
  value := ADOQuery.Fields[0].AsString;
  
  self.logger.debug('result : "' + value + '"');
  get := value;
end;

function TSATable.recordsList(pole : String): TStringList;
var
  value : String;
  ADOQuery : TADOQuery;
  tmp : TStringList;
begin
  tmp := TStringList.Create;

  ADOQuery := TADOQuery.Create(nil);
  ADOQuery.Connection := self.ADOConnection;
  ADOQuery.SQL.Clear;
  ADOQuery.SQl.Add('SELECT '+self.table_id+'.'+pole+' FROM '+self.table_id+';');
  ADOQuery.Active := true;
  ADOQuery.ExecSQL;

  ADOQuery.First;
  while not ADOQuery.EOF do begin
    value := ADOQuery.Fields[0].AsString;
    tmp.Add(value);
    ADOQuery.Next;
  end;
  recordsList := tmp;
end;

function TSATable.keys() : TStringList;
begin
  keys := self.recordsList(keyField());
end;

function TSATable.keyField() : String;
begin
  keyField := fieldsList[0];
end;

function TSATable.fieldsList() : TStringList;
var i : integer;
  tmp : TStringList;
begin
  tmp := TStringList.Create;
  self.ADOTable.Active := True;
  ADOTable.GetFieldNames(tmp);
  i := 0;
  self.ADOTable.Active := false;
  fieldsList := tmp;
end;


function TSATAble.tableValuesListToSQL(valuesCS : String) : String;
var
  keys, values : String;
  valuesList : TStringList;
  i : integer;
begin
  valuesList := TStringList.Create;
  valuesList.Delimiter := ',';
  valuesList.CommaText := StringReplace(valuesCS, ' ',  '^', [rfReplaceAll]);
  for i := 0 to valuesList.Count - 1 do begin
    if (i <> valuesList.Count - 1) then begin
      keys := keys + '[' + valuesList.Names[i] + '],';
      values := values + '"' + StringReplace(valuesList.ValueFromIndex[i], '^',  ' ', [rfReplaceAll]) + '",';
    end else begin
      keys := keys + '[' + valuesList.Names[i] + ']';
      values := values + '"' + StringReplace(valuesList.ValueFromIndex[i], '^',  ' ', [rfReplaceAll]) + '"';
    end;
  end;
  tableValuesListToSQL := '('+keys+')'+' VALUES ('+values+')';
end;

function TSATAble.tableValuesUpdateSQL(valuesCS : String) : String;
var
  valuesList : TStringList;

  r : String;
  i : integer;
begin
  valuesList := TStringList.Create;
  valuesList.Delimiter := ',';

  valuesList.CommaText := StringReplace(valuesCS, ' ',  '^', [rfReplaceAll]);
  for i := 0 to valuesList.Count - 1 do begin
    if (i <> valuesList.Count - 1) then begin
    //test_table.[password]
      r := r + self.table_id+'.[' + valuesList.Names[i] + ']="' + StringReplace(valuesList.ValueFromIndex[i], '^',  ' ', [rfReplaceAll]) + '", ';
        end else begin
      r := r + self.table_id+'.[' + valuesList.Names[i] + ']="' + StringReplace(valuesList.ValueFromIndex[i], '^',  ' ', [rfReplaceAll]) + '"';
    end;
  end;
  tableValuesUpdateSQL := r;
end;


function TSATable.existValue(field, value : String) : boolean;
var rList : TStringList;
begin
  rList := self.recordsList(field);
  if (rList.IndexOf(value) < 0) then existValue := false
  else existValue := true;
end;

function TSATable.existField(field: String) : boolean;
var fList : TStringList;
begin
  fList := self.fieldsList();
  if (fList.IndexOf(field) < 0) then existField := false
  else existField := true;
end;


procedure TSATable.syncToStringGrid(target_grid : TStringGrid; hide_key, titles : boolean);
var
  i, k : longword;
  key, field : String;
  v_offset, h_offset : integer;

  r_c : longint;
begin
  r_c := self.keys.Count;

  if (hide_key) then v_offset := -1 else v_offset := 0;
  if (titles) then h_offset := 1 else h_offset := 0;

  target_grid.ColCount := self.fieldsList.Count + v_offset;
  target_grid.RowCount := r_c + h_offset;

  if (r_c > 0) then begin
    for i := 0 to self.keys.Count - 1 do begin
     for k := 0 - v_offset to self.fieldsList.Count - 1 do begin
        key := self.keys().Strings[i];
        field := self.fieldsList.Names[k];
        field := self.fieldsList.Strings[k];
        target_grid.Cells[k + v_offset, i + h_offset] := self.get(key, field);
      end;
    end;
  end;
end;
end.
