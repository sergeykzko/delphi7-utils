unit SimpleAccess;

//  MIT License
//
//  Copyright (c) 2020 Sergey Kozubenko (SeaJay), Artem Muzichko
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.


interface
uses
  Contnrs, DB, ADODB, SATable, sysutils, Classes, Variants, ActiveX, ComObj, SimpleLogger;
type
  TSimpleAccess = Class(TObject)
  private
    uuid : String;
    login : String;
    path : String;
    coInit : boolean;
    ADOConnection : TADOConnection;

    logger : TSimpleLogger;
    function tableFieldsListToSQL(fields : TStringList) : String;
    procedure CreateAccessDatabase(filepath, password: String);
    procedure connect(path, password : String; coInit : boolean; exeMode : boolean);
    
  protected
  public
    constructor init(path, password : String; logger : TSimpleLogger; coInit : boolean); overload;
    constructor init(path : String; logger : TSimpleLogger; coInit : boolean); overload;

    function tableList : TStringList; overload;

    function createTable(table : String; CommaFields : String) : TSATable;
    function getTable(table : String) : TSATable;
    function existTable(table : String) : boolean;

    function getTableInit(table, CommaFields: String): TSATable;

//  TO-DO
//  procedure deleteTable(table : String);
//  createDB();
//  checkDB();
    procedure setNewPassword(currentPassword, newPassword: String);
    procedure Disconnection;
  published
  end;

implementation

//path to database Microsoft Access 2000 (*.mdb)

constructor TSimpleAccess.init(path, password : String; logger : TSimpleLogger; coInit : boolean);
begin
  self.logger := logger;
  self.path := path;
  self.coInit := coInit;

  self.connect(path, password, coInit, false);
end;

constructor TSimpleAccess.init(path : String; logger : TSimpleLogger; coInit : boolean);
begin
  self.init(path, '', logger, coInit);
end;


procedure TSimpleAccess.connect(path, password : String; coInit : boolean; exeMode : boolean);
begin
  if (self.coInit) then CoInitialize(nil);

  if (not FileExists(path)) then begin
    self.logger.warn('database file "' + path + '" not exists. Creating new DB.');
    CreateAccessDatabase(path, password);
  end;

  self.ADOConnection := TADOConnection.Create(nil);

  //https://www.connectionstrings.com/access/
  if (not exeMode) then begin
    if (password <> '') then self.ADOConnection.ConnectionString := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+path+';Jet OLEDB:Database Password=' + password + ';'
    else self.ADOConnection.ConnectionString := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+path+';Persist Security Info=False';
  end else begin
    self.ADOConnection.ConnectionString := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+path+';Mode=Share Deny Read|Share Deny Write;Jet OLEDB:Database Password=' + password + ';';
  end;
  self.ADOConnection.Open;
end;

function TSimpleAccess.tableFieldsListToSQL(fields : TStringList) : String;
var
  r : String;
  hkey, htype : String;
  thtype : String;
  i : Integer;
begin
  r := '';
  for i := 0 to fields.Count - 1 do begin
    hkey := fields.Names[i];
    htype := fields.ValueFromIndex[i];
    r := r +  '['+hkey+'] ' + htype + ', ';
  end;
  r := '(' + r + 'PRIMARY KEY(['+fields.Names[0]+']));';
  tableFieldsListToSQL := r;
end;

function TSimpleAccess.createTable(table : String; CommaFields : String) : TSATable;
var
  CREATE_TABLE : String;
  fields : TStringList;
begin
  self.logger.debug('creating table ' + table);
  if (not existTable(table)) then begin
    fields := TStringList.Create;
    fields.CommaText := CommaFields;
    CREATE_TABLE := 'CREATE TABLE ' + table + ' ' + tableFieldsListToSQL(fields);
    self.ADOConnection.Execute(CREATE_TABLE);
    createTable := TSATable.Create(self.ADOConnection, table, self.logger);
  end else begin
    self.logger.debug('error : table "' + table + '" is exist. New table not creted, return nil');
    createTable := nil;
  end;
end;


function TSimpleAccess.getTable(table : String) : TSATable;
begin
  if (existTable(table)) then begin
    self.logger.debug('getTable "' + table + '"');
    getTable := TSATable.Create(self.ADOConnection, table, self.logger);
  end else begin
    self.logger.debug('getTable "' + table + '" - no exist');
    getTable := nil;
  end;
end;

function TSimpleAccess.existTable(table : String) : boolean;
var
  r : TStringList;
begin
  if (tableList.IndexOf(table) < 0)
  then existTable := false
  else existTable := true;
end;


function TSimpleAccess.tableList : TStringList;
var
  r : TStringList;
  ADODataSet : TADODataSet;
begin
  r := TStringList.Create;
  ADODataSet := TADODataSet.Create(nil);
  try
    ADODataSet.Connection := self.ADOConnection;

    self.ADOConnection.OpenSchema(siTables, EmptyParam, EmptyParam, ADODataSet);
    ADODataSet.Filter := '(TABLE_TYPE = ''TABLE'')';
    ADODataSet.Filtered := True;
    while not ADODataSet.EOF do begin
      //self.logger.debug('-' + ADODataSet.FieldByName('TABLE_NAME').AsString);
      r.Add(ADODataSet.FieldByName('TABLE_NAME').AsString);
      ADODataSet.Next;
    end;
  finally
    ADODataSet.Free;
  end;
  tableList := r;
end;


procedure TSimpleAccess.Disconnection;
begin
  self.ADOConnection.Close;
  self.ADOConnection.Connected:=False;
  self.logger.info('DataBase "' + self.path + '" disconnected.');

  //SQL.EndUpdate;
  //ExecSQL;
  //Close;

  self.ADOConnection.Free;

  if (self.coInit) then CoUninitialize;
end;

function TSimpleAccess.getTableInit(table : String; CommaFields : String) : TSATable;
begin
  if (self.existTable(table)) then getTableInit := self.getTable(table)
  else getTableInit := self.createTable(table, CommaFields);
end;





procedure TSimpleAccess.CreateAccessDatabase(filepath, password: String);
var
  cat : OLEVariant;
  result : String;
begin
  result := '';
  try
    cat := CreateOleObject('ADOX.Catalog');

    if (password = '') then cat.create ('Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+filepath+';Jet OLEDB:Engine Type=4;')
    else cat.create ('Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+filepath+';Jet OLEDB:Database Password=' + password + ';Jet OLEDB:Encrypt Database=true;');

    cat := NULL;
  except
    on e : Exception do result := e.message;
  end;
end;

procedure TSimpleAccess.setNewPassword(currentPassword, newPassword : String);
var
  AdoCommand : TAdoCommand;
begin
  self.Disconnection;
  self.connect(self.path, currentPassword, self.coInit, true);
  AdoCommand := TAdoCommand.Create(nil);
  AdoCommand.Connection := self.ADOConnection;
  //AdoCommand.ConnectionString := self.ADOConnection.ConnectionString;
  AdoCommand.CommandText := 'alter database password '+ newPassword + ' ' + currentPassword;
  AdoCommand.Execute;
end;

end.
