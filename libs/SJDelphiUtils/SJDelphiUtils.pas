unit SJDelphiUtils;

//  MIT License
//
//  Copyright (c) 2020 Sergey Kozubenko (SeaJay)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.

interface
uses
  Forms, SysUtils, classes, DateUtils, Crypt.SHA1, StdCtrls, Grids;
type charset=set of char;

//id, crypt
function GenGUID(): String;
function RandomString(const ALength: Integer): String;
function password_hash(password : String) : String;
function password_hash_verify(password : String; hash : String) : Boolean;

//forms and windows
procedure CenterOnScreen(Form : TForm);
function getYSelStringGrid(targetStringGrid : TObject) : LongInt;
function getFirstValFromStringGrid(targetStringGrid : TObject) : String;
function getClicked(targetListBox : TObject) : String;

//and input filters (first try MaskEdit in 'Additional')
procedure EditKeyInputFilter(var field : string; var Key: Char; filter: charset; password : boolean);
function ValidateString(target : String; filter: charset) : boolean;
procedure editInputFilter(mode : String; editObj : TObject);

//types
function SJStrToBool (str : String) : boolean;
function SJBoolToStr (bool : boolean) : String;


//time
function UnixTimeNow() : LongWord;
function UnixTimeFromString(date : String) : LongWord;
function UnixTimeToString(UnixTime : LongWord) : String;

//debug
procedure PrintStringList(list : TStringList);

implementation

procedure CenterOnScreen(Form : TForm);
begin
  Form.Top := (screen.Height div 2) - (Form.Height div 2);
  Form.left := (screen.Width div 2) - (Form.Width div 2);
end;

procedure EditKeyInputFilter(var field : string; var Key: Char; filter: charset; password : boolean);
begin
if (key in filter) or (key = #8) then begin
    if (key <> #8) then begin
      field := field + key;
      if (password) then key := '*';
    end else delete(field, length(field), 1);
  end else key := #0;
end;

function StringListFromArray(arr : array of String) : TStringList;
var
  lst : TStringList;
  i : integer;
begin
  lst := TStringList.Create;
  for i := 0 to length(arr) - 1 do begin
    lst.add(arr[i]);
  end;
end;


function UnixTimeNow() : LongWord;
begin
  UnixTimeNow := DateTimeToUnix(Now());
end;

//Format : 18.05.2020 13:00:00
function UnixTimeFromString(date : String) : LongWord;
var
  fs: TFormatSettings;
begin
  //fs := TFormatSettings.Create;
  fs.DateSeparator := '.';
  fs.ShortDateFormat := 'dd.MM.yyyy';
  fs.TimeSeparator := ':';
  fs.ShortTimeFormat := 'hh:mm';
  fs.LongTimeFormat := 'hh:mm:ss';

  UnixTimeFromString := DateTimeToUnix(StrToDateTime(date, fs));
end;

function UnixTimeToString(UnixTime : LongWord) : String;
begin
  UnixTimeToString := DateTimeToStr(UnixToDateTime(UnixTime));;
end;

function ValidateString(target : String; filter: charset) : boolean;
var
  i : longint;
  ch : char;
  isClean : boolean;
begin
  isClean := true;
  for i:= 0 to Length (target) - 1 do begin
    ch := target[i];
    if not (ch in filter) then isClean := false;
  end;

  ValidateString := isClean;
end;


function GenGUID(): String;
var
  Uid: TGuid;
  r: HResult;
  t : String;
begin
  r := CreateGuid(Uid);
  if r = S_OK then begin
     t := StringReplace(GuidToString(Uid), '}',  '', [rfReplaceAll]);
     GenGUID := StringReplace(t, '{',  '', [rfReplaceAll])
  end else GenGUID := '';
end;

function RandomString(const ALength: Integer): String;
var
  i: Integer;
  LCharType: Integer;
begin
  Randomize;
  Result := '';
  for i := 1 to ALength do
  begin
    LCharType := Random(3);
    case LCharType of
      0: Result := Result + Chr(ord('a') + Random(26));
      1: Result := Result + Chr(ord('A') + Random(26));
      2: Result := Result + Chr(ord('0') + Random(10));
    end;
  end;
end;


function password_hash(password : String) : String;
var
  sail : String;
begin
  sail := RandomString(8);
  password_hash := SHA1(password + sail) + ':' + sail;
end;

function password_hash_verify(password : String; hash : String) : Boolean;
var
  clean_hash, new_hash, sail : String;
begin
  //password_hash := SHA1(password);

  clean_hash := Copy(hash, 1, Pos(':' ,hash)-1);
  sail := Copy(hash, Pos(':' ,hash)+1, Length(hash));

  new_hash := SHA1(password + sail) + ':' + sail;
  password_hash_verify := (new_hash = hash);
end;


procedure editInputFilter(mode : String; editObj : TObject);
var
  edit : TEdit;
  clean, source : String;
  i : Integer;
  ch : char;
begin
  edit := editObj as TEdit;

  clean := '';
  source := edit.Text;

  for i:= 0 to Length (source) do begin
    ch := source[i];

    if (mode = 'int') then begin
      if (ch in ['0'..'9']) then clean := clean + ch;
    end;
  end;

  edit.Text := clean;
end;

function getClicked(targetListBox : TObject) : String;
var
  list : TListBox;
  i, sel : Integer;
begin
  list := targetListBox as TListBox;

  for i:=0 to list.Items.Count -1 do begin
    if (list.Selected[i]) then begin
      writeln(IntToStr(i) + ' - selected');
      sel := i;
    end;
  end;
  getClicked := list.Items.ValueFromIndex[sel];
end;


function getFirstValFromStringGrid(targetStringGrid : TObject) : String;
var
  x, y : integer;
  sg : TStringGrid;
begin
  sg := targetStringGrid as TStringGrid;
  x := sg.Selection.Right;
  y := sg.Selection.Top;
  getFirstValFromStringGrid := sg.Cells[0, y];
end;

function getYSelStringGrid(targetStringGrid : TObject) : LongInt;
var
  x, y : integer;
  sg : TStringGrid;
begin
  sg := targetStringGrid as TStringGrid;
  x := sg.Selection.Right;
  getYSelStringGrid := sg.Selection.Top;
end;


function SJBoolToStr (bool : boolean) : String;

begin
  if (bool) then SJBoolToStr := 'true' else SJBoolToStr := 'false';
end;

function SJStrToBool (str : String) : boolean;
begin
  if (str = 'true') then SJStrToBool := true else SJStrToBool := false;
end;


procedure PrintStringList(list : TStringList);
var
  i : longword;
begin

   for i := 0 to list.Count -1 do writeln(' '+list.ValueFromIndex[i]);

end;

end.
