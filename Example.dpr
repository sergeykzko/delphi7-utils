program Example;

{$APPTYPE CONSOLE}

uses
  SimpleAccess in 'libs\SimpleAccess\SimpleAccess.pas',
  SATable in 'libs\SimpleAccess\SATable.pas',
  SlowStringMap in 'libs\SlowStringMap\SlowStringMap.pas',
  SimpleLogger in 'libs\SimpleLogger\SimpleLogger.pas',
  SJDelphiUtils in 'libs\SJDelphiUtils\SJDelphiUtils.pas',
  Crypt.SHA1 in 'libs\SHA1\Crypt.SHA1.pas',
  SysUtils;
var
  logger : TSimpleLogger;
  database : TSimpleAccess;

  testTable : TSATable;
  uuid : String;
  
procedure password(uuid : String);
var
  password, hash : String;
begin
  write('New Test Password :');
  readln(password);
  hash := password_hash(password);
  testTable.put(uuid, 'password=' + hash);
end;

procedure verify_password(uuid : String);
var
  password, hash : String;
begin
  writeln('[TEST LOGIN]');
  write('Input Your Password :');
  readln(password);
  hash := testTable.get(uuid, 'password');

  if (password_hash_verify(password, hash)) then writeln('[PASSWORD OK]') else writeln('[PASSWORD BAD]');
end;

procedure testCreateUser();
var
  uuid : String;
begin
  uuid := GenGUID();
  testTable.put(uuid, 'name=Tera Tester,created=' + IntToStr(UnixTimeNow()));

  logger.info('User Created!' + #10 + #13 + uuid + ' : ' + testTable.get(uuid, 'name') + ' : ' + UnixTimeToString(StrToInt(testTable.get(uuid, 'created'))));
  
  writeln;

  password(uuid);
  writeln;

  verify_password(uuid);
end;

begin
  logger := TSimpleLogger.Create('logs/');
  logger.stdoutEnable;
  logger.debugDisable;
  logger.info('hello!');

  database := TSimpleAccess.Init('test_database.mdb', 'qwerty123', logger, true);

  testTable := database.getTableInit('test_table', 'uuid=varchar,name=varchar,password=varchar,created=int');

  testCreateUser();

  PrintStringList(database.tableList);
  PrintStringList(testTable.recordsList('uuid'));
  //database.setNewPassword('qwerty123','123qwe');
  readln;
end.
